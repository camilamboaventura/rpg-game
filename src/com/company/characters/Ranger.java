package com.company.characters;

import com.company.PrimaryAttributes;
import com.company.items.*;

import static com.company.items.ArmorType.*;
import static com.company.items.WeaponType.*;

public class Ranger extends Character {
    public Ranger(String name) {
        super(name, 1, new PrimaryAttributes(8, 1, 7, 1) );
    }

    @Override
    public String functionality() {
        return null;
    }

    @Override
    public void levelUp() { //Every time a Ranger levels up, they gain:
        baseAttributes.setVitality(baseAttributes.getVitality() + 2);
        baseAttributes.setStrength(baseAttributes.getStrength() + 1);
        baseAttributes.setDexterity(baseAttributes.getDexterity() + 5);
        baseAttributes.setIntelligence(baseAttributes.getIntelligence() + 1);
    }

    @Override
    public double dps() {
        Weapon weapon = (Weapon) equipments.get(Slot.Weapon);
        return weapon.dps() * (1 + this.getTotalAttributes().getDexterity()/100) ; //character dps
    }

    @Override
    public boolean equip(Item item) throws InvalidArmorException, InvalidWeaponException {
        if (item instanceof Armor) { //
            var type = ((Armor) item).getType();
            if (type == Leather || type == Mail) {
                equipments.put(item.getSlot(), item);
            } else {
                throw new InvalidWeaponException();
            }

    }
        if (item instanceof Weapon) {
            var type = ((Weapon) item).getType();
            if (type != Bow) {
                throw new InvalidArmorException();
            }
            equipments.put(item.getSlot(), item);
        }
    return true;
    }
}
