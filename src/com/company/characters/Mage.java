package com.company.characters;

import com.company.PrimaryAttributes;
import com.company.items.*;

import static com.company.items.ArmorType.Cloth;
import static com.company.items.WeaponType.Staff;
import static com.company.items.WeaponType.Wand;

public class Mage extends Character {

    //constructor

    public Mage(String name) {
        super(name, 1, new PrimaryAttributes(5, 1, 1, 8));
    }

    //methods

    @Override
    public String functionality() {
        return null;
    }

    @Override
    public void levelUp() { //Every time a Mage levels up, they gain:
        baseAttributes.setVitality(baseAttributes.getVitality() + 3);
        baseAttributes.setStrength(baseAttributes.getStrength() + 1);
        baseAttributes.setDexterity(baseAttributes.getDexterity() + 1);
        baseAttributes.setIntelligence(baseAttributes.getIntelligence() + 5);
        level++;
    }

    @Override
    public double dps() {
        Weapon weapon = (Weapon) equipments.get(Slot.Weapon); //obtain equipment - weapon - from a slot
        return weapon.dps() * (1 + this.getTotalAttributes().getIntelligence()/100) ; //Character DPS
    }

    @Override
    public boolean equip(Item item) throws InvalidArmorException, InvalidWeaponException {
        if (item instanceof Armor) { // Is it an Armon?
            var type = ((Armor) item).getType(); //transform into an Armon and get his type
            if (type != Cloth) {
                throw new InvalidArmorException();
            }
            equipments.put(item.getSlot(), item); //add an item to slot
        }
        if (item instanceof Weapon) {
            var type = ((Weapon) item).getType();
            if (type == Staff || type == Wand) {
                equipments.put(item.getSlot(), item);
            } else {
                throw new InvalidWeaponException();
            }
        }
        return true;
    }
}
