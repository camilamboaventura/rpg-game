package com.company.characters;

import com.company.PrimaryAttributes;
import com.company.items.*;

import static com.company.items.ArmorType.*;
import static com.company.items.WeaponType.*;

public class Warrior extends Character {
    //constructor
    public Warrior(String name) {
        super(name, 1, new PrimaryAttributes(10, 5, 2, 1));
    }

    //methods

    @Override
    public String functionality() {
        return null;
    }

    @Override
    public void levelUp() { //Every time a Warrior levels up, they gain:
        baseAttributes.setVitality(baseAttributes.getVitality() + 5);
        baseAttributes.setStrength(baseAttributes.getStrength() + 3);
        baseAttributes.setDexterity(baseAttributes.getDexterity() + 2);
        baseAttributes.setIntelligence(baseAttributes.getIntelligence() + 1);
    }

    @Override
    public double dps() {
        Weapon weapon = (Weapon) equipments.get(Slot.Weapon); //getting the weapon from a slot
        double dps = 1;
        if (weapon != null) {
            dps = weapon.dps();
        }
        return dps * (1 + this.getTotalAttributes().getStrength() / 100); //Each point of strength increases a Warriors damage by 1%.
    }

    @Override
    public boolean equip(Item item) throws InvalidWeaponException, InvalidArmorException {
        if (item instanceof Armor) {
            var type = ((Armor) item).getType();
            if (type == Plate || type == Mail) {
                equipments.put(item.getSlot(), item);
            } else {
                throw new InvalidArmorException();
            }

            if (item.getRequiredLevel() > this.getLevel()) {
                throw new InvalidArmorException();
            }
        }
        if (item instanceof Weapon) {
            var type = ((Weapon) item).getType();
            if (type == Axe || type == Hammer || type == Sword) {
                equipments.put(item.getSlot(), item);
            } else {
                throw new InvalidWeaponException();
            }
            if (item.getRequiredLevel() > this.getLevel()) {
                throw new InvalidWeaponException();
            }
        }
        return true;
    }
}
