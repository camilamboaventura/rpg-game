package com.company.characters;

import com.company.PrimaryAttributes;
import com.company.items.*;

import java.util.HashMap;

public abstract class Character {
    // Instance Variables
    String name;
    int level;
    PrimaryAttributes baseAttributes;
    HashMap<Slot, Item> equipments;

    //constructor
    public Character(String name, int level, PrimaryAttributes baseAttributes) {
        this.name = name;
        this.level = level;
        this.baseAttributes = baseAttributes;
        this.equipments = new HashMap<>();
    }

    //methods
    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getLevel() {
        return level;
    }

    public void setLevel(int level) {
        this.level = level;
    }

    public PrimaryAttributes getBaseAttributes() {
        return baseAttributes;
    }

    public void setBaseAttributes(PrimaryAttributes baseAttributes) {
        this.baseAttributes = baseAttributes;
    }

    public HashMap<Slot, Item> getEquipments() {
        return equipments;
    }

    public void setEquipments(HashMap<Slot, Item> equipments) {
        this.equipments = equipments;
    }

    public abstract String functionality();
    public abstract void levelUp();
    public abstract double dps();
    public abstract boolean equip(Item item) throws InvalidArmorException, InvalidWeaponException;

    //charater Statistics
    public void statistics() {
        System.out.println("Name: " + name + ", level: " + level + ", strength: " + baseAttributes.getStrength() + ", dexterity: " + baseAttributes.getDexterity() + ", intelligence: " + baseAttributes.getIntelligence() + ", vitality: " + baseAttributes.getVitality() + ", dps: " + dps());
    }

    public PrimaryAttributes getTotalAttributes() {
        PrimaryAttributes totalAttributes = new PrimaryAttributes(baseAttributes.getVitality(), baseAttributes.getStrength(), baseAttributes.getDexterity(), baseAttributes.getIntelligence());
        for (Item item : this.equipments.values()) {
            if (item instanceof Armor) {//check if item is armor
                Armor armor = (Armor) item; //turning an item into an armor
                // Total attribute = base + attributes from all equipped armor
                totalAttributes.setIntelligence(totalAttributes.getIntelligence() + armor.getAttributes().getIntelligence());
                totalAttributes.setDexterity(totalAttributes.getDexterity() + armor.getAttributes().getDexterity());
                totalAttributes.setStrength(totalAttributes.getStrength() + armor.getAttributes().getStrength());
                totalAttributes.setVitality(totalAttributes.getVitality() + armor.getAttributes().getVitality());
            }
        }
        return totalAttributes;
    }
}
