package com.company.items;


/**
 * If a character tries to equip armor they should not be able to, either by it being the wrong type or being too high of a
 * level requirement, then a custom InvalidArmorException should be thrown
 */
public class InvalidArmorException extends Exception{
}
