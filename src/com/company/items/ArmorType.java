package com.company.items;

public enum ArmorType {
    Cloth, Leather, Mail, Plate
}
