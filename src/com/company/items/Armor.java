package com.company.items;

import com.company.PrimaryAttributes;

public class Armor extends Item{
    // Instance Variables
    ArmorType type;
    PrimaryAttributes attributes;

    //constructor
    public Armor(String name, int requiredLevel, Slot slot, ArmorType type, PrimaryAttributes attributes) {
        super(name, requiredLevel, slot);
        this.type = type;
        this.attributes = attributes;
    }


    //methods
    public ArmorType getType() {
        return type;
    }

    public void setType(ArmorType type) {
        this.type = type;
    }

    public PrimaryAttributes getAttributes() {
        return attributes;
    }
}
