package com.company.items;

public class Weapon extends Item{

    // Instance Variables
    int damage;
    double attackSpeed;
    WeaponType type;

    //constructor
    public Weapon(String name, int requiredLevel, Slot slot, int damage, double attackSpeed, WeaponType type) {
        super(name, requiredLevel, slot);
        this.damage = damage;
        this.attackSpeed = attackSpeed;
        this.type = type;
    }

    //methods
    public int getDamage() {
        return damage;
    }

    public void setDamage(int damage) {
        this.damage = damage;
    }

    public double getAttackSpeed() {
        return attackSpeed;
    }

    public void setAttackSpeed(double attackSpeed) {
        this.attackSpeed = attackSpeed;
    }

    public WeaponType getType() {
        return type;
    }

    public void setType(WeaponType type) {
        this.type = type;
    }

    public double dps() {
        return this.damage * this.attackSpeed;
    }
}
