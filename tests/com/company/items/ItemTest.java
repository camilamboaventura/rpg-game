package com.company.items;

import com.company.PrimaryAttributes;
import com.company.characters.Warrior;
import org.junit.jupiter.api.Test;
import static org.junit.jupiter.api.Assertions.assertEquals;

import static org.junit.jupiter.api.Assertions.assertThrows;

public class ItemTest {

    //If a character tries to equip a high level weapon, InvalidWeaponException should be thrown
    @Test
    void warrior_toEquipAHighLevelWeapon() {
        Warrior warrior = new Warrior("Brutus");
        Weapon weapon = testWeapon();
        weapon.setRequiredLevel(2);
        assertThrows(InvalidWeaponException.class, () -> warrior.equip(weapon) );
    }

    //If a character tries to equip a high level armor piece, InvalidArmorException should be thrown.
    @Test
    void warrior_toEquipHighLevelArmor() {
        Warrior warrior = new Warrior("Bob");
        Armor armor = testPlateBody();
        armor.setRequiredLevel(2);
        assertThrows(InvalidArmorException.class, () -> warrior.equip(armor) );
    }

    //If a character tries to equip the wrong weapon type, InvalidWeaponException should be thrown
    @Test
    void warrior_toEquipWrongWeaponType() {
        Warrior warrior = new Warrior("Jack");
        Weapon weapon = testWeapon();
        weapon.setType(WeaponType.Bow);
        assertThrows(InvalidWeaponException.class, () -> warrior.equip(weapon) );
    }

    //If a character tries to equip the wrong armor type, InvalidArmorException should be thrown.
    @Test
    void warrior_toEquipWrongArmorType() {
        Warrior warrior = new Warrior("Paul");
        Armor armor =testClothHead();
        armor.setType(ArmorType.Cloth);
        assertThrows(InvalidArmorException.class, () -> warrior.equip(armor) );
    }

    //If a character equips a valid weapon, a Boolean true should be returned
    @Test
    void warrior_equipValidWeapon() throws InvalidArmorException, InvalidWeaponException {
        Warrior warrior = new Warrior("John");
        Weapon weapon = testWeapon();
        Boolean expected = true;
        Boolean actual = warrior.equip(weapon);
        assertEquals(expected, actual);
    }

    //If a character equips a valid armor piece, a Boolean true should be returned.
    @Test
    void warrior_equipValidArmor() throws InvalidArmorException, InvalidWeaponException {
        Warrior warrior = new Warrior("Matt");
        Armor armor = testPlateBody();
        Boolean expected = true;
        Boolean actual = warrior.equip(armor);
        assertEquals(expected, actual);
    }

    //Calculate DPS if no weapon is equipped.
    @Test
    void warrior_calculateDPS_noWeaponEquipped() {
        Warrior warrior = new Warrior("Eric");
        double expected = 1*(1 + (5 / 100));
        double actual = warrior.dps();
        assertEquals(expected, actual);
    }

    //Calculate DPS with valid weapon equipped.
    @Test
    void warrior_calculateDPS_validWeaponEquipped() throws InvalidArmorException, InvalidWeaponException {
        Warrior warrior = new Warrior("Ed");
        Weapon weapon = testWeapon();
        warrior.equip(weapon);
        double expected =  (7 * 1.1)*(1 + (5 / 100));
        double actual = warrior.dps();
        assertEquals(expected, actual);
    }

    //Calculate DPS with valid weapon and armor equipped.
    @Test
    void warrior_calculateDPS_validWeaponAndArmorEquipped() throws InvalidArmorException, InvalidWeaponException {
        Warrior warrior = new Warrior("Ed");
        Armor armor = testPlateBody();
        Weapon weapon = testWeapon();
        warrior.equip(weapon);
        warrior.equip(armor);
        double expected =   (7 * 1.1) * (1 + ((5+1) / 100));
        double actual = warrior.dps();
        assertEquals(expected, actual);
    }

    private Weapon testWeapon() {
        Weapon testWeapon = new Weapon("Common Axe", 1, Slot.Weapon, 7, 1.1, WeaponType.Axe);
        return testWeapon;
    }

    private Armor testPlateBody() {
        Armor testPlateBody = new Armor("Common Plate Body Armor",1, Slot.Body, ArmorType.Plate, new PrimaryAttributes(1, 0, 0, 2));
        return testPlateBody;
    }

    private Weapon testBow() {
        Weapon testBow = new Weapon("Common Bow", 1, Slot.Weapon, 12, 0.8, WeaponType.Bow);
        return testBow;
    }

    private Armor testClothHead() {
        Armor testClothHead = new Armor("Common cloth Head Armor", 1, Slot.Head, ArmorType.Cloth, new PrimaryAttributes(0, 0, 5, 1));
        return testClothHead;
    }




}
