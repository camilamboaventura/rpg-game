package com.company.characters;

import com.company.PrimaryAttributes;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;

public class CharacterTests {


    //Verifying if a character is level 1 when created.
    @Test
    void create_executed_shouldBeLevelOne() {
        Mage mage = new Mage("Dumbledore");
        int expected = 1;
        int actual = mage.getLevel();
        assertEquals(expected, actual);
    }

    // Verifying if when a character gains a level, it should be level 2
    @Test
    void levelUp_executed_shouldBeLevelTwo() {
        Mage mage = new Mage("Voldemort");
        int expected = 2;
        mage.levelUp();
        int actual = mage.getLevel();
        assertEquals(expected, actual);
    }

    //  Verifying if each character class is created with the proper default attributes.

    @Test
    void create_mage_executed_shouldBeProperAttributes() {
        Mage mage = new Mage("Snape");
        PrimaryAttributes expected = new PrimaryAttributes(5, 1, 1, 8);
        PrimaryAttributes actual = mage.getBaseAttributes();
        assertEquals(expected, actual);
    }

    @Test
    void create_ranger_executed_shouldBeProperAttributes() {
        Ranger ranger = new Ranger("Deephorn");
        PrimaryAttributes expected = new PrimaryAttributes(8, 1, 7, 1);
        PrimaryAttributes actual = ranger.getBaseAttributes();
        assertEquals(expected, actual);
    }

    @Test
    void create_rogue_executed_shouldBeProperAttributes() {
        Rogue rogue = new Rogue("Boo");
        PrimaryAttributes expected = new PrimaryAttributes(8, 2, 6, 1);
        PrimaryAttributes actual = rogue.getBaseAttributes();
        assertEquals(expected, actual);
    }

    @Test
    void create_warrior_executed_shouldBeProperAttributes() {
        Warrior warrior = new Warrior("Ducan");
        PrimaryAttributes expected = new PrimaryAttributes(10, 5, 2, 1);
        PrimaryAttributes actual = warrior.getBaseAttributes();
        assertEquals(expected, actual);
    }

    //Verifying if each character class has their attributes increased when leveling up

    @Test
    void levelUp_mage_executed_attributesShouldIncrease() {
        Mage mage = new Mage("Snape");
        PrimaryAttributes expected = new PrimaryAttributes(8, 2, 2, 13);
        mage.levelUp();
        PrimaryAttributes actual = mage.getBaseAttributes();
        assertEquals(expected, actual);
    }

    @Test
    void levelUp_ranger_executed_attributesShouldIncrease() {
        Ranger ranger = new Ranger("Deephorn");
        PrimaryAttributes expected = new PrimaryAttributes(10, 2, 12, 2);
        ranger.levelUp();
        PrimaryAttributes actual = ranger.getBaseAttributes();
        assertEquals(expected, actual);
    }

    @Test
    void levelUp_rogue_executed_attributesShouldIncrease() {
        Rogue rogue = new Rogue("Ducan");
        PrimaryAttributes expected = new PrimaryAttributes(11, 3, 10, 2);
        rogue.levelUp();
        PrimaryAttributes actual = rogue.getBaseAttributes();
        assertEquals(expected, actual);
    }

    @Test
    void levelUp_warrior_executed_attributesShouldIncrease() {
        Warrior warrior = new Warrior("Ducan");
        PrimaryAttributes expected = new PrimaryAttributes(15, 8, 4, 2);
        warrior.levelUp();
        PrimaryAttributes actual = warrior.getBaseAttributes();
        assertEquals(expected, actual);
    }
}
