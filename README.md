![](RPG.png)

# RPG Characters

A Java console application that creates RPG characters.

## Requirements

a) Various character classes having attributes that increase at different rates as the character gains levels.

b) Equipment, such as armor and weapons, that character can equip. The equipped items will alter the power of the character, causing it to deal more damage and be able to survive longer. Certain characters can equip certain item types.

c) Custom exceptions. There are two custom exceptions you are required to write, as detailed in Appendix B.

d) Full test coverage of the functionality. Some testing data is provided, it can be used to complete the assignment in a test-driven development manner.

* Check "Java RPG Characters.pdf" to see more details.